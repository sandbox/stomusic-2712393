<?php

/**
 * @file
 * Contains \Drupal\push_notifications\Form\PushNotificationsMessageForm.
 */

namespace Drupal\push_notifications\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\push_notifications\Controller\PushNotificationsController;

class PushNotificationsMessageForm extends FormBase {
  public function getFormId() {
    return 'push_notifications_message_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
      $values = $form_state->getValues();

      $form['message'] = array(
          '#type' => 'fieldset',
          '#title' => t('Push Notification Message'),
          '#description' => t('Compose the elements of your push notification message (@limit bytes max.)', array(
              '@limit' => PUSH_NOTIFICATIONS_APNS_PAYLOAD_SIZE_LIMIT,
          )),
          '#tree' => TRUE,
      );

      $form['message']['title'] = array(
          '#type' => 'textfield',
          '#title' => t('Message subject'),
          '#description' => t('Compose the subject to send out (@limit characters max.)', array(
              '@limit' => 40,
          )),
          '#default_value' => (isset($values['message']['alert'])) ? $values['message']['alert'] : '',
          '#required' => TRUE,
          '#size' => 128,
          '#maxlength' => 40,
          '#weight' => 10,
      );

      $form['message']['alert'] = array(
          '#type' => 'textfield',
          '#title' => t('Message text'),
          '#description' => t('Compose the message to send out (@limit characters max.)', array(
              '@limit' => PUSH_NOTIFICATIONS_APNS_PAYLOAD_SIZE_LIMIT,
          )),
          '#default_value' => (isset($values['message']['alert'])) ? $values['message']['alert'] : '',
          '#required' => TRUE,
          '#size' => 128,
          '#maxlength' => PUSH_NOTIFICATIONS_APNS_PAYLOAD_SIZE_LIMIT,
          '#weight' => 10,
      );

      //TODO push_notifications_used_languages
//      if (\Drupal::moduleHandler()->moduleExists('locale')) {
//          $form['language'] = array(
//              '#type' => 'select',
//              '#title' => t('Language'),
//              '#options' => push_notifications_used_languages(),
//              '#description' => t('Optionally, only select the push notifications to recipients with this language.'),
//              '#default_value' => (isset($values['language'])) ? $values['language'] : '',
//              '#weight' => 12,
//          );
//      }

      // Only show Android option if C2DM credentials are available.
      $recipients_options = array('ios' => t('iOS (iPhone/iPad)'));
      if ((PUSH_NOTIFICATIONS_C2DM_USERNAME && PUSH_NOTIFICATIONS_C2DM_PASSWORD) || PUSH_NOTIFICATIONS_GCM_API_KEY) {
          $recipients_options['android'] = t('Android');
      }
      $form['recipients'] = array(
          '#type' => 'checkboxes',
          '#title' => t('Recipients'),
          '#description' => t('Select the recipients for this push message'),
          '#options' => $recipients_options,
      );

      $form['submit'] = array(
          '#type' => 'submit',
          '#value' => 'Send Push Notification',
          '#weight' => 50,
      );

      return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
      $values = $form_state->getValues();
      $recipients = $values['recipients'];

      // Add all "message" elements to the payload.
      // Other modules can alter the contents of the payload
      // array by adding additional elements to 'message'
      // when it implements hook_form_alter.
      // Store payload in the form_state.
      $form_state->setValue('payload', $values['message']);

      // Make sure at least one recipient (group) is selected.
      if (empty($recipients['ios']) && empty($recipients['android'])) {
          $form_state->setErrorByName('recipients', t('No message was sent. Please select at least one recipient group.'));
      }
      $push = new PushNotificationsController();
      // Validate that the message size is ok.
      if (!$push->push_notifications_check_payload_size($values['message'])) {
          $form_state->setErrorByName('message', t('Your message exceeds the allowed size of @max_size bytes. Please shorten your message.', array(
              '@max_size' => PUSH_NOTIFICATIONS_APNS_PAYLOAD_SIZE_LIMIT,
          )));
      }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
      $push = new PushNotificationsController();
      $values = $form_state->getValues();
      $recipients = $values['recipients'];
      $payload = $values['payload'];

      $language = (isset($values['language'])) ? $values['language'] : false;

      // Send message to all iOS recipients.
      if (!empty($recipients['ios'])) {
          // Get all iOS recipients.
          $tokens_ios = $push->push_notifications_get_tokens(array(
              'type_id' => PUSH_NOTIFICATIONS_TYPE_ID_IOS,
              'language' => $language,
          ));
          if (!empty($tokens_ios)) {
              // Convert the payload into the correct format for APNS.
              $payload_apns = array('aps' => $payload);
              $result = $push->push_notifications_apns_send_message($tokens_ios, $payload_apns);
              $dsm_type = ($result['success']) ? 'status' : 'error';
              drupal_set_message($result['message'], $dsm_type);
          }
          else {
              drupal_set_message(t('No iOS recipients found, potentially for this language.'));
          }
      }

      // Send message to all Android recipients.
      if (!empty($recipients['android'])) {
          // Get all Android recipients.
          $tokens_android = $push->push_notifications_get_tokens(array(
              'type_id' => PUSH_NOTIFICATIONS_TYPE_ID_ANDROID,
              'language' => $language,
          ));
          if (!empty($tokens_android)) {
              // Determine which method to use for Google push notifications.
              switch (PUSH_NOTIFICATIONS_GOOGLE_TYPE) {
                  case PUSH_NOTIFICATIONS_GOOGLE_TYPE_C2DM:
                      $result = $push->push_notifications_c2dm_send_message($tokens_android, $payload);
                      break;

                  case PUSH_NOTIFICATIONS_GOOGLE_TYPE_GCM:
                      $result = $push->push_notifications_gcm_send_message($tokens_android, $payload);
                      break;
              }
              $dsm_type = ($result['success']) ? 'status' : 'error';
              drupal_set_message($result['message'], $dsm_type);
          }
          else {
              drupal_set_message(t('No Android recipients found, potentially for this language.'));
          }
      }
  }

//
//  protected function getEditableConfigNames() {
//    return ['push_notifications.message'];
//  }
}
