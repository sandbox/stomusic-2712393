<?php

/**
 * @file
 * Contains \Drupal\push_notifications\Form\PushNotificationsTokenAddForm.
 */

namespace Drupal\push_notifications\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\push_notifications\Controller\PushNotificationsController;

class PushNotificationsTokenAddForm extends FormBase {
  public function getFormId() {
    return 'push_notifications_token_add_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
      $form['token'] = array(
          '#type' => 'textfield',
          '#title' => t('Device token'),
          '#default_value' => '',
          '#required' => TRUE,
          '#maxlength' => 255
      );
      $form['type'] = array(
          '#title' => t('Device type'),
          '#type' => 'select',
          '#options' => array(
              PUSH_NOTIFICATIONS_TYPE_ID_ANDROID => t('Android'),
              PUSH_NOTIFICATIONS_TYPE_ID_IOS => t('iOS (iPhone/iPad)')
          ),
          '#default_value' => 'android',
          '#required' => TRUE,
      );
      $form['uid'] = array(
          '#type' => 'entity_autocomplete',
          '#target_type' => 'user',
          '#title' => t('Device user'),
          '#required' => TRUE,
      );

      //TODO push_notifications_used_languages
//      $form['language'] = array(
//              '#type' => 'select',
//              '#title' => t('Language'),
//              '#options' => push_notifications_used_languages(),
//              '#default_value' => (isset($values['language'])) ? $values['language'] : '',
//              '#weight' => 12,
//          );


      $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Add device to list'),
          '#weight' => 50,
      );

      return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
      $values = $form_state->getValues();
      $push = new PushNotificationsController();
      $result = $push->push_notifications_store_token($values['token'], $values['type'], $values['uid']);
      if($result) {
          drupal_set_message(t('Success'));
      }
  }

//
//  protected function getEditableConfigNames() {
//    return ['push_notifications.message'];
//  }
}
