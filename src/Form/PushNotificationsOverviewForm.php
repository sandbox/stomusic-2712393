<?php

/**
 * @file
 * Contains \Drupal\push_notifications\Form\PushNotificationsOverviewForm.
 */

namespace Drupal\push_notifications\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class PushNotificationsOverviewForm extends ConfigFormBase {
  public function getFormId() {
    return 'push_notifications_overview_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
      // Load all tokens stored in the database.
      $query = db_select('push_notifications_tokens', 'pnt');
      $query->fields('pnt', array('uid', 'type', 'language'));
      $result = $query->execute();
      $counts = array(
          'total' => 0,
          'ios' => 0,
          'android' => 0,
          'anonymous' => 0,
          'registered' => 0,
      );

      foreach ($result as $data) {
          $counts['total']++;
          switch ($data->type) {
              case PUSH_NOTIFICATIONS_TYPE_ID_IOS: $counts['ios']++; break;
              case PUSH_NOTIFICATIONS_TYPE_ID_ANDROID: $counts['android']++; break;
          }
          if ($data->uid == 0) {
              $counts['anonymous']++;
          }
          else {
              $counts['registered']++;
          }
      }

      $form = array();

      $statistics_description = (!$counts['total']) ? t('No tokens stored yet.') : null;
      $form['statistics'] = array(
          '#type' => 'fieldset',
          '#title' => 'Push Notification Statistics',
          '#description' => $statistics_description,
      );

      $form['statistics']['total'] = array(
          '#type' => 'item',
          '#title' => t('Total Tokens Stored'),
          '#markup' => $counts['total'],
      );

      $form['statistics']['ios'] = array(
          '#type' => 'item',
          '#title' => t('iOS Tokens Stored'),
          '#markup' => $counts['ios'],
      );

      $form['statistics']['android'] = array(
          '#type' => 'item',
          '#title' => t('Android Tokens Stored'),
          '#markup' => $counts['android'],
      );

      $form['statistics']['anonymous'] = array(
          '#type' => 'item',
          '#title' => t('Anonymous Tokens Stored'),
          '#markup' => $counts['anonymous'],
      );

      $form['statistics']['registered'] = array(
          '#type' => 'item',
          '#title' => t('Registered Tokens Stored'),
          '#markup' => $counts['registered'],
      );

      //TODO push_notifications_used_languages
//      $form['languages'] = array(
//          '#type' => 'fieldset',
//          '#title' => 'Push Notification Languages',
//          '#description' => t('Languages used across all push notification recipients'),
//      );
//
//      // Determine which languages are used.
//      $languages_used = push_notifications_used_languages();
//      if (!empty($languages_used)) {
//          unset($languages_used[0]);
//          $languages_markup = theme('item_list', array('items' => $languages_used));
//      }
//      else {
//          $languages_markup = t('No tokens stored or no tokens with a language association stored');
//      }
//
//      $form['languages']['used'] = array(
//          '#type' => 'item',
//          '#markup' => $languages_markup,
//      );

    return parent::buildForm($form, $form_state);
  }

  protected function getEditableConfigNames() {
    return ['push_notifications.config'];
  }
}
