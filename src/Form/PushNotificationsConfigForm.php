<?php

/**
 * @file
 * Contains \Drupal\push_notifications\Form\PushNotificationsConfigForm.
 */

namespace Drupal\push_notifications\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\push_notifications\Controller\PushNotificationsController;
use Drupal\Core\Url;
use Drupal\Component\Utility\SafeMarkup;

class PushNotificationsConfigForm extends ConfigFormBase {
  public function getFormId() {
    return 'push_notifications_config_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
      // Add JavaScript for Google push notification selector.
      $build['#attached'] = array(
          'js' => drupal_get_path('module', 'push_notifications') . '/scripts/push_notifications_admin.js'
      );

      $form['configuration_general'] = array(
          '#type' => 'fieldset',
          '#title' => t('General Push Notifications Settings'),
      );

      $form['configuration_general']['push_notifications_require_enabled_language'] = array(
          '#type' => 'checkbox',
          '#title' => t('Only allow enabled languages'),
          '#description' => t('When setting a device token, only allow languages that are enabled on this site (instead of all valid languages)'),
          '#default_value' => \Drupal::config('push_notifications.config')->get('require_enabled_language') ? : 0,
      );

//      $replacements_privatemsg = array(
//          '!privatemsg' => l('PrivateMSG', 'http://drupal.org/project/privatemsg/', array('html' => true, 'attributes' => array('target' => '_blank'))),
//          '!privatemsg_status' => (\Drupal::moduleHandler()->moduleExists('privatemsg')) ? 'successfully installed' : 'not installed',
//      );
//      $form['configuration_general']['push_notifications_privatemsg_integration'] = array(
//          '#type' => 'checkbox',
//          '#title' => t('Enable integration with PrivateMSG module'),
//          '#description' => t('Check this to deliver messages sent through the !privatemsg module to all recipients with at least one valid device token. Requires !privatemsg module 7.x-1.x (status: !privatemsg_status)', $replacements_privatemsg),
//          '#default_value' => variable_get('push_notifications_privatemsg_integration', 0),
//      );

      $configuration_apns_replacements = array(
          '@link' => \Drupal::l(t('this tutorial'), Url::fromUri('http://blog.boxedice.com/2009/07/10/how-to-build-an-apple-push-notification-provider-server-tutorial/'), array('attributes' => array('target' => '_blank'))),
          '@cert_name_development' => 'apns-development' . PUSH_NOTIFICATIONS_APNS_CERTIFICATE_RANDOM . '.pem',
          '@cert_name_production' => 'apns-production' . PUSH_NOTIFICATIONS_APNS_CERTIFICATE_RANDOM . '.pem',
      );
      $form['configuration_apns'] = array(
          '#type' => 'fieldset',
          '#title' => t('Apple Push Notifications'),
          '#description' => t('Configure Push Notifications for Apple\'s Push Notification Server. Select your environment. Both environments require the proper certificates in the \'certificates\' folder of this module.<br />The filename for the development certificate should be \'@cert_name_development\', the production certificate should be \'@cert_name_production\'. See @link for instructions on creating certificates.', $configuration_apns_replacements),
      );

      if (\Drupal::config('push_notifications.config')->get('apns_certificate_random') ? : '' == '') {
          $regenerate_certificate_string_description = t('Your certificate name is currently set to the default. To increase security, click here to create a custom name for your APNS certificates. Please note that you will have to change the filenames for both certificate files afterwards.');
      }
      else {
          $regenerate_certificate_string_description = t('You are already using a random certificate name. Click here to create a new random name for your APNS certificates. Please note that you will have to update the filenames for both certificate files accordingly.');
      }
      $form['configuration_apns']['regenerate_certificate_string_description'] = array(
          '#type' => 'item',
          '#title' => t('APNS Certificate Name'),
          '#markup' => $regenerate_certificate_string_description,
      );

      $form['configuration_apns']['regenerate_certificate_string'] = array(
          '#type' => 'submit',
          '#value' => t('Generate new certificate string'),
          '#submit' => array('::pushNotificationsRegenerateCertificateStringSubmit'),
      );

      $form['configuration_apns']['push_notifications_apns_environment'] = array(
          '#type' => 'select',
          '#title' => t('APNS Environment'),
          '#description' => t('Select the active APNS Environment. Please note that development certificates do not work with apps released in the Apple app store; production certificates only work with apps released in the app store.'),
          '#options' => array(
              0 => 'Development',
              1 => 'Production',
          ),
          '#default_value' => \Drupal::config('push_notifications.config')->get('apns_environment') ? : 0,
      );

      $form['configuration_apns']['stream_context_limit'] = array(
          '#type' => 'select',
          '#title' => t('Stream Context Limit'),
          '#description' => t('Defines the amount of messages sent per stream limit, i.e. how many notifications are sent per connection created with Apple\'s servers. The higher the limit, the faster the message delivery. If the limit is too high, messages might not get delivered at all. Unclear (to me) what Apple\'s <em>actual</em> limit is.'),
          '#options' => array_combine(array(1, 5, 10, 25, 50),array(1, 5, 10, 25, 50)),
          '#default_value' => \Drupal::config('push_notifications.config')->get('apns_stream_context_limit') ? : 1,
      );

      $form['configuration_apns']['passphrase'] = array(
          '#type' => 'textfield',
          '#title' => t('Passphrase'),
          '#description' => t('If your APNS certificate has a passphrase, enter it here. Otherwise, leave this field blank.'),
          '#default_value' => \Drupal::config('push_notifications.config')->get('apns_passphrase') ? : '',
      );

      $form['configuration_apns']['push_notifications_apns_certificate_folder'] = array(
          '#type' => 'textfield',
          '#title' => t('APNS Certificate Folder Path'),
          '#description' => t('The preferred location for the certificate files is a folder outside of your web root, i.e. a folder not accessible through the Internet. Specify the full path here, e.g. \'/users/danny/drupal_install/certificates/\'. If you are using the \'certificates\' folder within the module directory, leave this field blank.'),
          '#default_value' => \Drupal::config('push_notifications.config')->get('apns_certificate_folder') ? : '',
      );

      $form['configuration_c2dm'] = array(
          '#type' => 'fieldset',
          '#title' => t('Google Push Notifications'),
          '#description' => t('Requires a valid Google Cloud Messaging or C2DM account. @signup for Google Cloud Messaging. Google does not accept new signups for C2DM.', array('@signup' => \Drupal::l(t('Signup here'), Url::fromUri('http://developer.android.com/guide/google/gcm/gs.html'), array('attributes' => array('target' => '_blank'))))),
      );

      $form['configuration_c2dm']['push_notifications_google_type'] = array(
          '#type' => 'radios',
          '#title' => t('Google Push Notifications Type'),
          '#description' => t('Google\'s C2DM service (Cloud 2 Device Messaging) was replaced by the Google Cloud Messaging Service. Each services requires a different authentication method. '),
          '#default_value' => PUSH_NOTIFICATIONS_GOOGLE_TYPE,
          '#options' => array(
              PUSH_NOTIFICATIONS_GOOGLE_TYPE_C2DM => 'Cloud 2 Device Messaging',
              PUSH_NOTIFICATIONS_GOOGLE_TYPE_GCM => 'Google Cloud Messaging',
          ),
      );

      $c2dm_credentials_visible = (PUSH_NOTIFICATIONS_GOOGLE_TYPE == PUSH_NOTIFICATIONS_GOOGLE_TYPE_C2DM);
      $form['configuration_c2dm']['c2dm_credentials'] = array(
          '#type' => 'fieldset',
          '#title' => t('Cloud 2 Device Messaging Settings'),
          '#description' => t('Enter your C2DM credentials.'),
          '#attributes' => array(
              'style' => (PUSH_NOTIFICATIONS_GOOGLE_TYPE != PUSH_NOTIFICATIONS_GOOGLE_TYPE_C2DM) ? 'display:none' : '',
          ),
      );

      $form['configuration_c2dm']['c2dm_credentials']['push_notifications_c2dm_username'] = array(
          '#type' => 'textfield',
          '#title' => t('C2DM Username'),
          '#description' => t('Enter the username for your C2DM Google Account'),
          '#default_value' => \Drupal::config('push_notifications.config')->get('c2dm_username') ? : '',
      );

      $form['configuration_c2dm']['c2dm_credentials']['push_notifications_c2dm_password'] = array(
          '#type' => 'textfield',
          '#title' => t('C2DM Password'),
          '#description' => t('Enter the password for your C2DM Google Account'),
          '#default_value' => \Drupal::config('push_notifications.config')->get('c2dm_password') ? : '',
      );

      $form['configuration_c2dm']['gcm_credentials'] = array(
          '#type' => 'fieldset',
          '#title' => t('Google Cloud Messaging'),
          '#description' => t('Enter your Google Cloud Messaging details'),
          '#attributes' => array(
              'style' => (PUSH_NOTIFICATIONS_GOOGLE_TYPE != PUSH_NOTIFICATIONS_GOOGLE_TYPE_GCM) ? 'display:none' : '',
          ),

      );

      $form['configuration_c2dm']['gcm_credentials']['push_notifications_gcm_api_key'] = array(
          '#type' => 'textfield',
          '#title' => t('Google Cloud Messaging API Key'),
          '#description' => t('Enter the API key for your Google Cloud project'),
          '#default_value' => \Drupal::config('push_notifications.config')->get('gcm_api_key') ? : '',
      );

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
      // If custom certificate directory is set, ensure the directory exists.
      $values = $form_state->getValues();
      $custom_dir = $values['push_notifications_apns_certificate_folder'];
      if (!empty($custom_dir)) {
          if (!file_exists(SafeMarkup::checkPlain($custom_dir))) {
              $form_state->setErrorByName('push_notifications_apns_certificate_folder', t('Custom certificate directory does not exist. Please create the path before saving your configuration.'));
          }
      }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
      // Module settings.
      $values = $form_state->getValues();
      \Drupal::configFactory()->getEditable('push_notifications.config')->set('require_enabled_language', $values['push_notifications_require_enabled_language'])->save();

//      variable_set('push_notifications_privatemsg_integration', $values['push_notifications_privatemsg_integration']);

      $apns_environment = $values['push_notifications_apns_environment'];
      // Set the variables for the apns development environment.
      \Drupal::configFactory()->getEditable('push_notifications.config')->set('apns_environment', $apns_environment)->save();

      switch ($apns_environment) {
          // Development Environment.
          case 0:
              // Set the individual variables.
              \Drupal::configFactory()->getEditable('push_notifications.config')->set('apns_host', 'gateway.sandbox.push.apple.com')->save();
              \Drupal::configFactory()->getEditable('push_notifications.config')->set('apns_certificate', 'apns-development' . PUSH_NOTIFICATIONS_APNS_CERTIFICATE_RANDOM . '.pem')->save();
              break;
          case 1:
              // Set the individual variables.
              \Drupal::configFactory()->getEditable('push_notifications.config')->set('apns_host', 'gateway.push.apple.com')->save();
              \Drupal::configFactory()->getEditable('push_notifications.config')->set('apns_certificate', 'apns-production' . PUSH_NOTIFICATIONS_APNS_CERTIFICATE_RANDOM . '.pem')->save();
              break;
      }

      // Set C2DM credentials.
      \Drupal::configFactory()->getEditable('push_notifications.config')->set('c2dm_username', $values['push_notifications_c2dm_username'])->save();
      \Drupal::configFactory()->getEditable('push_notifications.config')->set('c2dm_password', $values['push_notifications_c2dm_password'])->save();

      // Set GCM API key.
      \Drupal::configFactory()->getEditable('push_notifications.config')->set('gcm_api_key', $values['push_notifications_gcm_api_key'])->save();

      // Set the APNS certificate location.
      $apns_cert_folder = SafeMarkup::checkPlain($values['push_notifications_apns_certificate_folder']);
      if (!empty($apns_cert_folder)) {
          // Add a trailing slash if not present.
          if (substr($apns_cert_folder, -1) != DIRECTORY_SEPARATOR) {
              $apns_cert_folder .= DIRECTORY_SEPARATOR;
          }
          \Drupal::configFactory()->getEditable('push_notifications.config')->set('apns_certificate_folder', $apns_cert_folder)->save();
      }
      else {
          \Drupal::configFactory()->getEditable('push_notifications.config')->delete('apns_certificate_folder');
      }

      // Check if the certificate exists.
      $push = new PushNotificationsController();
      $apns_cert = $push->_push_notifications_get_apns_certificate();
      if (!file_exists($apns_cert)) {
          drupal_set_message(t('Could not find any APNS certificates at @path. Please ensure your certificates are located in the correct folder to send push notifications and are named correctly.', array('@path' => $apns_cert)), 'warning', FALSE);
      }

      // Set the APNS pem file passphrase.
      \Drupal::configFactory()->getEditable('push_notifications.config')->set('apns_passphrase', $values['passphrase'])->save();

      // Set the APNS stream limit.
      \Drupal::configFactory()->getEditable('push_notifications.config')->set('apns_stream_context_limit', $values['stream_context_limit'])->save();

      $replacements = array(
          '@environment' => ($apns_environment) ? "Production" : "Development",
      );
      drupal_set_message(t('The APNS environment was successfully set to "@environment".', $replacements));
  }

  /**
   * Form callback for regenerating the APNS certificate string.
   */
    public function pushNotificationsRegenerateCertificateStringSubmit(array $form, FormStateInterface $form_state) {
        _push_notifications_set_random_certificate_string();
        drupal_set_message('The names for your APNS certificates were successfully changed. Please rename both certificate files.');
    }

  protected function getEditableConfigNames() {
    return ['push_notifications.configure'];
  }
}
