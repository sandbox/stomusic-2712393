<?php

/**
 * @file
 * Contains \Drupal\push_notifications\Plugin\views\field\DeviceType.
 */

namespace Drupal\push_notifications\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\FieldPluginBase;

/**
 * Render a device type id as string value
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("device_type")
 */

class DeviceType extends FieldPluginBase {

    /**
     * {@inheritdoc}
     */
    protected function defineOptions() {
        $options = parent::defineOptions();
        $options['type_format'] = array('default' => 'type_name');
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state) {
        $form['type_format'] = array(
            '#type' => 'select',
            '#title' => t('Output format'),
            '#options' => array(
                'type_name' => t('Token Type Name'),
                'type_id' => t('Token Type ID'),
            ),
            '#default_value' => $this->options['type_format'],
        );
        parent::buildOptionsForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitOptionsForm(&$form, FormStateInterface $form_state) {
        // Merge plural format options into one string and drop the individual
        // option values.
        $options = &$form_state->getValue('options');
        $options['format_plural_string'] = implode(LOCALE_PLURAL_DELIMITER, $options['format_plural_values']);
        unset($options['format_plural_values']);
        parent::submitOptionsForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function render(ResultRow $values) {
        $token_type_id = $this->getValue($values);

        if ($this->options['type_format'] == 'type_id') {
            return $token_type_id;
        }

        return $this->get_token_type_name($token_type_id);
    }

    function get_token_type_name($type){
        // Build an array of types, as defined by the Push Notifications module.
        $valid_types = array(
            PUSH_NOTIFICATIONS_TYPE_ID_IOS => 'iOS',
            PUSH_NOTIFICATIONS_TYPE_ID_ANDROID => 'Android',
        );
        if (isset($valid_types[$type])){
            return $valid_types[$type];
        }
        return '';
    }

    public function get_token_type_options(){
        return array(
            'type_name' => t('Token Type Name'),
            'type_id' => t('Token Type ID'),
        );
    }
}
