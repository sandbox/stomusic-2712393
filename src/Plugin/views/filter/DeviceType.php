<?php

/**
 * @file
 * Contains \Drupal\views\Plugin\views\filter\DeviceType.
 */

namespace Drupal\push_notifications\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\InOperator;

/**
 * Simple filter to handle matching of multiple options selectable via checkboxes
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("device_type")
 */
class DeviceType extends InOperator {

    /**
     * Child classes should be used to override this function and set the
     * 'value options', unless 'options callback' is defined as a valid function
     * or static public method to generate these values.
     *
     * This can use a guard to be used to reduce database hits as much as
     * possible.
     *
     * @return array|NULL
     *   The stored values from $this->valueOptions.
     */
    public function getValueOptions() {
        if (!isset($this->valueOptions)) {
            $options = array(
                PUSH_NOTIFICATIONS_TYPE_ID_IOS => 'iOS',
                PUSH_NOTIFICATIONS_TYPE_ID_ANDROID => 'Android',
            );
            $this->valueOptions = $options;
        }
        return $this->valueOptions;
    }

}
