<?php
/**
 * @file
 * Push Notifications functionality to drupal 8.
 */

/**
 * Constants Definition.
 */
//
// Device Types.
//
define('PUSH_NOTIFICATIONS_TYPE_ID_IOS', \Drupal::config('push_notifications.config')->get('type_id_ios') ? : 0);
define('PUSH_NOTIFICATIONS_TYPE_ID_ANDROID', \Drupal::config('push_notifications.config')->get('type_id_anroid') ? : 1);

//
// Apple Variables
//
// Apple Server
define('PUSH_NOTIFICATIONS_APNS_HOST', \Drupal::config('push_notifications.config')->get('apns_host') ? : 'gateway.push.apple.com');
// Apple Server port.
define('PUSH_NOTIFICATIONS_APNS_PORT', 2195);
// Apple Feedback Server, initially set to development server.
define('PUSH_NOTIFICATIONS_APNS_FEEDBACK_HOST', \Drupal::config('push_notifications.config')->get('apns_feedback_host') ? : 'feedback.push.apple.com');
// Apple Feedback Server port.
define('PUSH_NOTIFICATIONS_APNS_FEEDBACK_PORT', 2196);
// Random suffix for certificate string.
define('PUSH_NOTIFICATIONS_APNS_CERTIFICATE_RANDOM', \Drupal::config('push_notifications.config')->get('apns_certificate_random') ? : '');
// Name of certificate, initially set to development certificate.
define('PUSH_NOTIFICATIONS_APNS_CERTIFICATE', \Drupal::config('push_notifications.config')->get('apns_certificate') ? : 'apns-production' . PUSH_NOTIFICATIONS_APNS_CERTIFICATE_RANDOM . '.pem');
// Size limit for individual payload, in bytes.
define('PUSH_NOTIFICATIONS_APNS_PAYLOAD_SIZE_LIMIT', 2048);
// Payload sound
define('PUSH_NOTIFICATIONS_APNS_NOTIFICATION_SOUND', \Drupal::config('push_notifications.config')->get('apns_notification_sound') ? : 'default');
// Boolean value to indicate wether Apple's feedback service should be called
// on cron to remove unused tokens from our database.
define('PUSH_NOTIFICATIONS_APNS_QUERY_FEEDBACK_SERVICE', \Drupal::config('push_notifications.config')->get('apns_query_feedback_service') ? : 1);
// Maximum of messages to send per stream context.
define('PUSH_NOTIFICATIONS_APNS_STREAM_CONTEXT_LIMIT', \Drupal::config('push_notifications.config')->get('apns_stream_context_limit') ? : 1);


//
// Google Variables
//
// Google Push Notification Types
// 0 => Cloud 2 Device Messaging
// 1 => Google Cloud Messaging
define('PUSH_NOTIFICATIONS_GOOGLE_TYPE_C2DM', 0);
define('PUSH_NOTIFICATIONS_GOOGLE_TYPE_GCM', 1);
define('PUSH_NOTIFICATIONS_GOOGLE_TYPE', \Drupal::config('push_notifications.config')->get('google_type') ? : PUSH_NOTIFICATIONS_GOOGLE_TYPE_GCM);

//
// C2DM Variables
//
// C2DM Credentials.
define('PUSH_NOTIFICATIONS_C2DM_USERNAME', \Drupal::config('push_notifications.config')->get('c2dm_username') ? : '');
define('PUSH_NOTIFICATIONS_C2DM_PASSWORD', \Drupal::config('push_notifications.config')->get('c2dm_password') ? : '');
define('PUSH_NOTIFICATIONS_C2DM_CLIENT_LOGIN_ACTION_URL', \Drupal::config('push_notifications.config')->get('c2dm_client_login_action_url') ? : 'https://www.google.com/accounts/ClientLogin');
// C2DM Server Post URL
define('PUSH_NOTIFICATIONS_C2DM_SERVER_POST_URL', \Drupal::config('push_notifications.config')->get('c2dm_server_post_url') ? :'https://android.apis.google.com/c2dm/send');

//
// GCM Variables
//
// GCM API KEY Credentials.
define('PUSH_NOTIFICATIONS_GCM_API_KEY', \Drupal::config('push_notifications.config')->get('gcm_api_key') ? : '');
// GCM Server Post URL
define('PUSH_NOTIFICATIONS_GCM_SERVER_POST_URL', \Drupal::config('push_notifications.config')->get('gcm_server_post_url') ? : 'https://android.googleapis.com/gcm/send');


/**
 * Determine any languages used in the push
 * notifications table.
 */
//TODO Problem with _locale_get_predefined_list()
//function push_notifications_used_languages() {
//    $query = db_select('push_notifications_tokens', 'pnt');
//    $query->fields('pnt', array('language'));
//    $query->distinct();
//    $result = $query->execute();
//
//    // Convert the records into an array with
//    // full language code available.
//    include_once DRUPAL_ROOT . '/includes/iso.inc';
//    $languages = _locale_get_predefined_list();
//
//    $used_languages = array();
//    foreach ($result as $record) {
//        $used_languages[$record->language] = $languages[$record->language][0];
//    }
//
//    if (!empty($used_languages)) {
//        // Sort the languages alphabetically.
//        $used_langauges = asort($used_languages);
//        // Add an "All" option.
//        array_unshift($used_languages, 'All Recipients');
//    }
//
//    return $used_languages;
//}

/**
 * Generate and set the random file ending for APNS certificates.
 */
function _push_notifications_set_random_certificate_string() {
    // Generate a random 10-digit string.
    $random_string = substr(md5(microtime()), 0, 10);
    \Drupal::configFactory()->getEditable('push_notifications.config')->set('apns_certificate_random', '-' . $random_string)->save();
}