<?php

/**
* Expose Push Notifications data to views.
*/
function push_notifications_views_data() {
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['push_notifications_tokens']['table'] = array(
    'group' => t('Push Notifications'),
    'title' => t('Push Tokens'),
    'help' => t('List of stored push tokens.'),
  );

  // Define this as a base table.
  $data['push_notifications_tokens']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Push Token'),
    'help' => t("Push tokens are unique identifiers for an app."),
    'weight' => -10,
  );

  // Push Token.
  $data['push_notifications_tokens']['token'] = array(
      'title' => t('Token'),
      'help' => t('Push Token.'),
      'field' => array(
          'id' => 'standard',
      ),
      'argument' => array(
          'id' => 'string',
      ),
      'filter' => array(
          'id' => 'string',
      ),
      'sort' => array(
          'id' => 'standard',
      ),
  );

  // User ID associated with this token.
  $data['push_notifications_tokens']['uid'] = array(
      'title' => t('User ID Token Owner'),
      'help' => t('UID associated with this token'),
      'field' => array(
          'id' => 'numeric',
      ),
      'filter' => array(
          'id' => 'numeric',
      ),
      'argument' => array(
          'id' => 'numeric',
      ),
      'search' => array(
          'id' => 'standard',
      ),
      'relationship' => array(
          'title' => t('User'),
          'help' => t('The user on which the log entry as written.'),
          'base' => 'users',
          'base field' => 'uid',
          'id' => 'standard',
      ),
  );

  // Device Type.
  $data['push_notifications_tokens']['type'] = array(
      'title' => t('Device Type'),
      'help' => t('Device Type (iOS or Android)'),
      'field' => array(
          'id' => 'device_type',
      ),
      'filter' => array(
          'id' => 'device_type',
      ),
      'argument' => array(
          'id' => 'numeric',
      ),
      'search' => array(
          'id' => 'standard',
      ),
  );

  // Timestamp.
  $data['push_notifications_tokens']['timestamp'] = array(
      'title' => t('Timestamp'),
      'help' => t('Timestamp this token was created / last updated.'),
      'field' => array(
          'id' => 'date',
      ),
      'argument' => array(
          'id' => 'date',
      ),
      'filter' => array(
          'id' => 'date',
      ),
      'sort' => array(
          'id' => 'date',
      ),
  );

  // TODO Language.
//  if (\Drupal::moduleHandler()->moduleExists('locale')) {
//    $data['push_notifications_tokens']['language'] = array(
//      'title' => t('Language'),
//      'help' => t('The language the comment is in.'),
//      'field' => array(
//        'handler' => 'views_handler_field_locale_language',
//        'click sortable' => TRUE,
//      ),
//      'filter' => array(
//        'handler' => 'views_handler_filter_locale_language',
//      ),
//      'argument' => array(
//        'handler' => 'views_handler_argument_locale_language',
//      ),
//      'sort' => array(
//        'handler' => 'views_handler_sort',
//      ),
//    );
//  }

  return $data;
}
